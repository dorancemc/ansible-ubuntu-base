ansible-ubuntu-base
=========
Base configuration for ubuntu box

Requirements
------------

None

Role Variables
--------------


Dependencies
------------


Example Playbook
----------------

    - hosts: ubuntu-servers
      roles:
         - { role: ansible-ubuntu-base }

License
-------

APACHE 2.0

Author Information
------------------

Dorance Martinez
dorancemc@gmail.com